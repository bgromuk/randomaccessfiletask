package com.company;

import java.io.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Main {

    public static final String TEST_TXT_FILE = "test.txt";

    public static void main(String[] args) {

        writeInitialDataToFile();

        ExecutorService executor = Executors.newFixedThreadPool(5);
        for (int i = 0; i < 10; i++) {
            Runnable worker = new RandomAccessFileSimpleThread(i*20);
            executor.execute(worker);
        }
        executor.shutdown();
        while (!executor.isTerminated()) {
        }
        System.out.println("Finished all threads");

        printResultToConsole();
    }

    private static void writeInitialDataToFile() {
        try {
            // create a new RandomAccessFile with filename test
            RandomAccessFile raf = new RandomAccessFile(TEST_TXT_FILE, "rw");

            // write something in the file
            raf.writeUTF("Hello World");
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    private static void printResultToConsole() {
        FileReader fileReader = null;
        try {
            fileReader = new FileReader(new File(TEST_TXT_FILE));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        BufferedReader bufferedReader = new BufferedReader(fileReader);
        try {

            System.out.println(bufferedReader.readLine());

        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

}
