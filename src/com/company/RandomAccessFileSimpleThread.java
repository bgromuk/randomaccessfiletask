package com.company;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;

import static com.company.Main.TEST_TXT_FILE;

public class RandomAccessFileSimpleThread implements Runnable {
    private long seekValue;

    public RandomAccessFileSimpleThread(long seekValue) {
        this.seekValue = seekValue;
    }

    @Override
    public void run() {
        // create a new RandomAccessFile with filename test
        RandomAccessFile raf = null;
        try {
            raf = new RandomAccessFile(TEST_TXT_FILE, "rw");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        // write something in the file
        try {

            // set the file pointer
            raf.seek(seekValue);

            // write something in the file
            raf.writeUTF("This is an example");

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
